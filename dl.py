from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import wget
import ssl

import time
import sys
import os

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")   
# chrome_driver = '/usr/bin/chromedriver'

namayava = sys.argv[0]
#kwd = namayava.replace(' ', '-').replace('(', '').replace(')', '').replace(':', '').lower()
#print(kwd)

path = input('Where do you want to download your anime? ...Please enter the absolute path.')
os.chdir(path)

class animeotaku():

    def __init__(self, url):
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.choice = 0
        self.driver.get(url)

    def takequery(self):
        query = input('Which anime do you intend to download?')
        return query

    def search(self, query):
        #Search for the Query
        print('Processing')
        search_element = self.driver.find_element_by_id('keyword')
        search_element.clear()
        search_element.send_keys(query)
        search_element.send_keys(Keys.RETURN)
        enter_button_element = self.driver.find_element_by_xpath('//*[@id="search-form"]/div[1]/input[2]')
        enter_button_element.click()
        html_list = self.driver.find_element_by_xpath('//*[@id="wrapper_bg"]/section/section[1]/div/div[2]/ul')
        items = html_list.find_elements_by_tag_name('img')
        return items

    def print_result(self, res):
        for i in range(len(res)):
            print(i+1, '    =>    ', res[i].get_attribute('alt'))

    def narrow_down(self, res):
        self.choice = int(input('Please enter your choice...Onegai...'))
        self.choice = self.choice - 1
        res[self.choice].click()

    def get_episodes(self):
        self.driver.find_element_by_xpath('//*[@id="episode_related"]/li[1]/a').click()
        url = self.driver.find_element_by_xpath('//*[@id="wrapper_bg"]/section/section[1]/div[1]/div[2]/div[3]/a').get_attribute('href')
        return url


    def get_all_urls(self, text):
        lst = text.split('+')
        n = int(lst[-1])
        all_urls = list()
        for i in range(1, n+1):
            all_urls.append('+'.join(lst[:-1]) + str(i))
        return all_urls
       
shinpachi = animeotaku('https://gogoanimes.co/')
query = shinpachi.takequery()
results = shinpachi.search(query)
shinpachi.print_result(results)
shinpachi.narrow_down(results)
url = shinpachi.get_episodes()
all_urls = shinpachi.get_all_urls(url)

#print(all_urls)
#print(len(all_urls))

#Make and enter a folder with the name of the desired anime...W
os.makedirs(query)
os.chdir(query)

#Actual Download Procedure...
for u in all_urls:
    d = webdriver.Chrome(chrome_options=chrome_options)
    d.get(u)
    address = d.find_element_by_xpath('//*[@id="main"]/div/div[2]/div/div[4]/div/a').get_attribute('href')
    ssl._create_default_https_context = ssl._create_unverified_context
    wget.download(address)




    ##Legacy
    # req = Request('https://gogoanimes.co/search.html?keyword='+str(kwd))
    # response = urlopen(req).read()
    ##Overrided urllib as mozilla for security reasons...
    # class AppURLopener(urllib.request.FancyURLopener):
    #     version = "Mozilla/5.0"
    # opener = AppURLopener()
    # response = opener.open('https://gogoanimes.co/category/'+str(kwd))
    # print(dir(response))
    # print('Response code: ', response.code)




